<?php

/**********************************
 * INTEGRATIONS LIST 
 *********************************/
function docusign_connect_integrations_list_form($form, &$form_state)
{
  $integrations_list_config = variable_get('docusign_connect_integrations_list_config', array());

  // Build the sortable table header.
  $header = array(
    'name' => t('Integration Name'),
    'selection' => t('Selection Configuration'),
    'plugins' => t('Plugin Configuration'),
    'delete' => t('Delete'),
  );

  //Build the rows.
  $options = array();
  foreach ($integrations_list_config as $key => $ic)
  {
    $options[$key] = array(
      'name' => $ic['name'],
      'selection' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => 'Selection Config',
          '#href' => 'admin/config/docusign_connect/selection/' . $key,
        ),
      ),
      'plugins' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => 'Plugin Config',
          '#href' => 'admin/config/docusign_connect/plugins/' . $key,
        ),
      ),
      'delete' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => 'Delete',
          '#href' => 'admin/config/docusign_connect/delete/' . $key,
        ),
      ),
    );
  }

  //Build the tableselect.
  $form['nodes'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No integrations defined.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Add New Integration',
  );

  return $form;
}

function docusign_connect_integrations_list_form_validate($form, &$form_state)
{
}

function docusign_connect_integrations_list_form_submit($form, &$form_state)
{
  drupal_goto(url('admin/config/docusign_connect/new_integration', array('absolute' => TRUE)));
}

function docusign_connect_integration_delete($int_id)
{
  $integrations_list_config = variable_get('docusign_connect_integrations_list_config', array());
  if(isset($integrations_list_config[$int_id]))
  {
    unset($integrations_list_config[$int_id]);
  }
  $integrations_list_config = array_values($integrations_list_config);
  variable_set('docusign_connect_integrations_list_config', $integrations_list_config);
}

/**********************************
 * ADD NEW INTEGRATION
 *********************************/
function docusign_connect_add_new_integration_form($form, &$form_state)
{
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => 'Integration Name',
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  return $form;
}

function docusign_connect_add_new_integration_form_validate($form, &$form_state)
{
}

function docusign_connect_add_new_integration_form_submit($form, &$form_state)
{
  docusign_connect_add_new_integration($form_state['values']['name']);
  drupal_goto(url('admin/config/docusign_connect', array('absolute' => TRUE)));
}

function docusign_connect_add_new_integration($name)
{
  $integrations_list_config = variable_get('docusign_connect_integrations_list_config', array());
  $integrations_list_config[]['name'] = $name;
  variable_set('docusign_connect_integrations_list_config', $integrations_list_config);
  return count($integrations_list_config) - 1;
}

/**********************************
 * SELECTION CONFIGURATION
 *********************************/
function docusign_connect_selection_configuration($form, &$form_state, $int_id)
{
  $integrations_list_config = variable_get('docusign_connect_integrations_list_config', array());
  if(!isset($integrations_list_config[$int_id]))
  {
    form_set_error('ERROR: you have not selected a valid integration!');
    return;
  }
  $integration = $integrations_list_config[$int_id];

  $form['TemplateId'] = array(
    '#type' => 'textfield',
    '#title' => 'Template ID',
    '#default_value' => isset($integration['selection']['TemplateId']) ? $integration['selection']['TemplateId'] : '',
    '#description' => 'If you wish to enter multiple values, separate each value with a comma',
  );

  $form['TemplateName'] = array(
    '#type' => 'textfield',
    '#title' => 'Template Name',
    '#default_value' => isset($integration['selection']['TemplateName']) ? $integration['selection']['TemplateName'] : '',
    '#description' => 'If you wish to enter multiple values, separate each value with a comma',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  return $form;
}

function docusign_connect_selection_configuration_validate($form, &$form_state)
{
}

function docusign_connect_selection_configuration_submit($form, &$form_state)
{
  $selection_data = array(
    'TemplateId' => explode(',', $form_state['values']['TemplateId']),
    'TemplateName' => explode(',', $form_state['values']['TemplateName']),
  );
  docusign_connect_add_selection_configuration($form_state['build_info']['args'][0], $selection_data);
  drupal_goto(url('admin/config/docusign_connect', array('absolute' => TRUE)));
}

function docusign_connect_add_selection_configuration($int_id, $selection_data)
{
  $integrations_list_config = variable_get('docusign_connect_integrations_list_config', array());
  foreach($selection_data as $key => $value)
  {
    $integrations_list_config[$int_id]['selection'][$key] = $value;
  }
  variable_set('docusign_connect_integrations_list_config', $integrations_list_config);
}

/**********************************
 * PLUGIN CONFIGURATION
 *********************************/
function docusign_connect_plugin_configuration($form, &$form_state, $int_id)
{
  $plugins = docusign_connect_collect_plugin_info();
  $integrations_list_config = variable_get('docusign_connect_integrations_list_config', array());
  if(!isset($integrations_list_config[$int_id]))
  {
    form_set_error('ERROR: you have not selected a valid integration!');
    return;
  }
  $integration = $integrations_list_config[$int_id];

  // Build the sortable table header.
  $header = array(
    'name' => t('Plugin'),
    'configure' => t('Configure'),
    'delete' => t('Delete'),
  );

  //Build the rows.
  $options = array();
  if(isset($integration['plugins']))
  {
    foreach ($integration['plugins'] as $key => $p)
    {
      $options[$key] = array(
        'name' => $plugins[$p['id']]['name'],
        'configure' => array(
          'data' => array(
            '#type' => 'link',
            '#title' => 'Config',
            '#href' => 'admin/config/docusign_connect/plugins/' . $int_id . '/' . $p['id'] . '_config' . '/' . $key,
          ),
        ),
        'delete' => array(
          'data' => array(
            '#type' => 'link',
            '#title' => 'Delete',
            '#href' => 'admin/config/docusign_connect/plugins/' . $int_id . '/delete/' . $key,
          ),
        ),
      );
    }
  }

  //Build the tableselect.
  $form['plugins'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No plugins defined.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Add New Plugin',
  );

  return $form;
}

function docusign_connect_plugin_configuration_validation($form, &$form_state)
{
}

function docusign_connect_plugin_configuration_submit($form, &$form_state)
{
  drupal_goto(url('admin/config/docusign_connect/plugins/' . $form_state['build_info']['args'][0] . '/add_plugin', array('absolute' => TRUE)));
}


function docusign_connect_plugin_delete($int_id, $plugin_id)
{
  $integrations_list_config = variable_get('docusign_connect_integrations_list_config', array());
  if(isset($integrations_list_config[$int_id]) && isset($integrations_list_config[$int_id]['plugins'][$plugin_id]))
  {
    unset($integrations_list_config[$int_id]['plugins'][$plugin_id]);
  }
  $integrations_list_config[$int_id]['plugins'] = array_values($integrations_list_config[$int_id]['plugins']);
  variable_set('docusign_connect_integrations_list_config', $integrations_list_config);
}

/**********************************
 * ADD A PLUGIN 
 *********************************/
function docusign_connect_plugin_configuration_add_form($form, &$form_state, $int_id)
{
  $plugins = docusign_connect_collect_plugin_info();
  $options = array();
  foreach($plugins as $name => $p)
  {
    $options[$name] = $p['name'];
  }

  $form['plugin'] = array(
    '#type' => 'select',
    '#title' => 'Choose Plugin',
    '#options' => $options,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  return $form;
}

function docusign_connect_plugin_configuration_add_form_validate($form, &$form_state)
{
}

function docusign_connect_plugin_configuration_add_form_submit($form, &$form_state)
{
  docusign_connect_add_plugin($form_state['build_info']['args'][0], $form_state['values']['plugin']);
  drupal_goto(url('admin/config/docusign_connect/plugins/' . $form_state['build_info']['args'][0], array('absolute' => TRUE)));
}

function docusign_connect_add_plugin($int_id, $plugin_id)
{
  $integrations_list_config = variable_get('docusign_connect_integrations_list_config', array());
  $integrations_list_config[$int_id]['plugins'][] = array(
    'id' => $plugin_id,
  );
  variable_set('docusign_connect_integrations_list_config', $integrations_list_config);
  return count($integrations_list_config[$int_id]['plugins']) - 1;
}

/**********************************
 * CONFIGURE A PLUGIN 
 *********************************/
function docusign_connect_plugin_configuration_config_plugin($form, &$form_state, $int_id, $plugin_id)
{
  $integrations_list_config = variable_get('docusign_connect_integrations_list_config', array());
  if(!isset($integrations_list_config[$int_id]))
  {
    form_set_error('ERROR: you have not selected a valid integration!');
    return;
  }
  $integration = $integrations_list_config[$int_id];

  if(!isset($integration['plugins'][$plugin_id]))
  {
    form_set_error('ERROR: you have not selected a valid plugin!');
    return;
  }
  $plugin_name = $integration['plugins'][$plugin_id]['id'];
  
  $plugins = docusign_connect_collect_plugin_info();
  $form = drupal_get_form($plugins[$plugin_name]['config_form_id'], $int_id, $plugin_id);

  return $form;
}

/**********************************
 * AJAX CALLBACKS
 *********************************/
function _docusign_connect_integrations_list_form_ajax($form, &$form_state)
{
  return $form;
}

