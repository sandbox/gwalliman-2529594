jQuery(function($) {
  $(document).ready(function() {
    if($('#docusign_connect_appxtender_configuration_wrapper .form-item-file-selection-all input').prop('checked')) {
      $('#docusign_connect_appxtender_configuration_wrapper .form-item-file-selection input').val('');
      $('#docusign_connect_appxtender_configuration_wrapper .form-item-file-selection input').prop("disabled", true); 
      $('#docusign_connect_appxtender_configuration_wrapper .form-item-file-selection input').css('background-color', '#ccc');
    }
  });

  $('#docusign_connect_appxtender_configuration_wrapper .form-item-file-selection-all input').change(function() {
    if(this.checked)
    {
      $('#docusign_connect_appxtender_configuration_wrapper .form-item-file-selection input').val('');
      $('#docusign_connect_appxtender_configuration_wrapper .form-item-file-selection input').prop("disabled", true); 
      $('#docusign_connect_appxtender_configuration_wrapper .form-item-file-selection input').css('background-color', '#ccc');
    }
    else
    {
      $('#docusign_connect_appxtender_configuration_wrapper .form-item-file-selection input').prop("disabled", false); 
      $('#docusign_connect_appxtender_configuration_wrapper .form-item-file-selection input').css('background-color', '#fff');
    }
  });
});
