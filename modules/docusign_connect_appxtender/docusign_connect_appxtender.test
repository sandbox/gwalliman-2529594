<?php
module_load_include('inc', 'docusign_connect', 'docusign_connect.admin');
//module_load_include('inc', 'appxtender_api', 'appxtender_api.admin');

class DocusignConnectAppXtenderTestCase extends DrupalWebTestCase
{
  public $appx_configs = array();

  public static function getInfo()
  {
    return array(
      'name' => 'Docusign Connect AppXtender Test Case',
      'description' => 'Tests the various functionalities of the Docusign Connect AppXtender module',
      'group' => 'Docusign Connect',
    );
  }

  public function setUp()
  {
    //$this->appx_configs = variable_get('appxtender_api_config', array());
    parent::setUp(array('docusign_connect_appxtender'));
  }

  public function testDocusignConnectAppXtenderEndpointCall()
  {
    //appxtender_api_set_configs($this->appx_configs['appx_url'], $this->appx_configs['appx_username'], $this->appx_configs['appx_password']);

    $filename = realpath(".") . '/' . drupal_get_path('module', 'docusign_connect') . '/test/envelope_1.xml';
    $handle = fopen($filename, "r", 1);
    $contents = fread($handle, filesize($filename));
    fclose($handle);

    $int_id = docusign_connect_add_new_integration('Test Integration');
    $selection_configuration = array(
      'TemplateName' => array(
        'AppXtender Test',
      ),
    );
    docusign_connect_add_selection_configuration($int_id, $selection_configuration);
    $plugin_id = docusign_connect_add_plugin($int_id, 'docusign_connect_appxtender');

    $datasource = 'ax-sfa';
    $application = 'STUDENT_TEST';
    $file_selection = '0';
    $file_selection_all = null;
    $mappings = array(
      array(
        'docusign' => 'fields',
        'selector' => 'DLN',
        'selected_docusign' => 'TabValue',
        'appxtender' => 'DLN',
      ),
      array(
        'docusign' => 'fields',
        'selector' => 'AID_YEAR',
        'selected_docusign' => 'TabValue',
        'appxtender' => 'AID_YEAR',
      ),
      array(
        'docusign' => 'fields',
        'selector' => 'LAST_NAME',
        'selected_docusign' => 'TabValue',
        'appxtender' => 'LAST_NAME',
      ),
      array(
        'docusign' => 'fields',
        'selector' => 'FIRST_NAME',
        'selected_docusign' => 'TabValue',
        'appxtender' => 'FIRST_NAME',
      ),
      array(
        'docusign' => 'fields',
        'selector' => 'DOCTYPE',
        'selected_docusign' => 'TabValue',
        'appxtender' => 'DOCTYPE',
      ),
      array(
        'docusign' => 'fields',
        'selector' => 'SOURCE',
        'selected_docusign' => 'TabValue',
        'appxtender' => 'SOURCE',
      ),
      array(
        'docusign' => 'fields',
        'selector' => 'EMPLID',
        'selected_docusign' => 'TabValue',
        'appxtender' => 'EMPLID',
      )
    );

    docusign_connect_appxtender_add_plugin_configuration($int_id, $plugin_id, $datasource, $application, $file_selection, $file_selection_all, $mappings);
    $integrations_list_config = variable_get('docusign_connect_integrations_list_config', array());

    /*********************
     * Confirm that the POSTFIELDS are built correctly
     ********************/
    $envelope = docusign_connect_build_envelope_object($contents);
    $postfields = docusign_connect_appxtender_build_postfields($int_id, $plugin_id, $datasource, $application, $envelope);

    $postfields_metadata = $postfields;
    $postfields_files = $postfields;
    
    $postfield_keys = array_keys($postfields_metadata);
    foreach($postfield_keys as $key)
    {
      if(strpos($key, 'File') !== FALSE)
      {
        unset($postfields_metadata[$key]);
      }
      else
      {
        unset($postfields_files[$key]);
      }
    }

    $expected_postfields = array(
      'DLN' => '123412341234',
      'AID_YEAR' => '2012',
      'LAST_NAME' => 'Walliman',
      'FIRST_NAME' => 'Garret',
      'DOCTYPE' => 'BIRTH_CERTIFICATE',
      'SOURCE' => 'SCAN',
      'EMPLID' => '1201569607',
    );

    $this->assertEqual(
      $postfields_metadata,
      $expected_postfields,
      'Postfields metadata contains all metadata with correct values'
    );

    $this->assertEqual(
      array('File0'),
      array_keys($postfields_files),
      'Postfields contains the proper number of files and proper file key(s)' 
    );

    $this->assertNotEqual(
      FALSE,
      strpos($postfields['File0'], '1.pdf'),
      'Postfields file attachments contain the proper file(s)'
    );

    /*********************
     * Confirm that "POST All Files" works as intended
     ********************/
    $file_selection = null;
    $file_selection_all = 1;
    docusign_connect_appxtender_add_plugin_configuration($int_id, $plugin_id, $datasource, $application, $file_selection, $file_selection_all, $mappings);

    $envelope = docusign_connect_build_envelope_object($contents);
    $postfields = docusign_connect_appxtender_build_postfields($int_id, $plugin_id, $datasource, $application, $envelope);

    $postfields_metadata = $postfields;
    $postfields_files = $postfields;
    
    $postfield_keys = array_keys($postfields_metadata);
    foreach($postfield_keys as $key)
    {
      if(strpos($key, 'File') !== FALSE)
      {
        unset($postfields_metadata[$key]);
      }
      else
      {
        unset($postfields_files[$key]);
      }
    }

    $expected_postfields = array(
      'DLN' => '123412341234',
      'AID_YEAR' => '2012',
      'LAST_NAME' => 'Walliman',
      'FIRST_NAME' => 'Garret',
      'DOCTYPE' => 'BIRTH_CERTIFICATE',
      'SOURCE' => 'SCAN',
      'EMPLID' => '1201569607',
    );

    $this->assertEqual(
      $postfields_metadata,
      $expected_postfields,
      'Postfields metadata contains all metadata with correct values'
    );

    $this->assertEqual(
      array('File0', 'File1'),
      array_keys($postfields_files),
      'Postfields contains the proper number of files and proper file key(s)' 
    );

    $this->assertNotEqual(
      FALSE,
      strpos($postfields['File0'], '1_0.pdf'),
      'Postfields file attachments contain the proper file(s)'
    );

    $this->assertNotEqual(
      FALSE,
      strpos($postfields['File1'], '2_0.pdf'),
      'Postfields file attachments contain the proper file(s)'
    );

    /*********************
     * Confirm that we can properly POST our items to AppXtender
     ********************/

    /*$execution_id = time();

    global $base_url;
    $url = $base_url . "/docusignConnect/endpoint/$execution_id";
    $method = 'POST';
    $options = array(
      'method' => $method,
      'data' => $contents,
    );

    $result = drupal_http_request($url, $options);

    $this->assertEqual(
      $result->code,
      200,
      'Sending a properly configured request to the endpoint results in a 200 response code'
    );

    $position = strpos($result->data, "Received. Ran integration $int_id.") !== FALSE;

    $this->assertNotEqual(
      $position,
      FALSE,
      'Sending a properly configured request to the endpoint results in proper response text'
    );

    $execution_log = variable_get('docusign_connect_execution_log', array());*/
  }
}

